const tabItems = document.getElementsByClassName("tab-item");
const tabContent = document.getElementsByClassName("tab-content");

const currentNews = [
  {
    "image": "assets/img/im_01.jpg",
    "date": "03.07.2015",
    "title": "Einladung zur Jahrespressekonferenz",
    "text": "",
    "more": "#"
  },
  {
    "image": "assets/img/im_02.jpg",
    "date": "03.07.2015",
    "title": "Studienstart im Juni",
    "text": "Studie zu rheumatoider Arthritis (RA)",
    "more": "#"
  },
  {
    "image": "assets/img/im_03.jpg",
    "date": "03.07.2015",
    "title": "Herzenssache - Patienten warten auf ein Spenderherz",
    "text": "TV-Tipp: Ein Beitrag in der WDR Mediathek",
    "more": "#"
  },
];

const eventNews = [
  {
    "image": "assets/img/evt_01.jpg",
    "date": "16.12.2020",
    "title": "Alljährliches Kickerturnier",
    "text": "",
    "more": "#"
  }
];

/**
 * Loads the current list.
 * @param {string} tab - The tab element.
 */
function loadList(tab) {
  // console.log(tab);
  switch (tab) {
    case 'current':
      let currentNewsList = document.getElementById("current-news-list");
      currentNews.forEach(listItem => {
        currentNewsList.appendChild(getListItem(listItem));
      });
      break;
    case 'events':
      let eventNewsList = document.getElementById("events-news-list");
      eventNews.forEach(listItem => {
        eventNewsList.appendChild(getListItem(listItem));
      });
      break;

    default:
      break;
  }
}

/**
 * Removes all nodes of an HTMLElement.
 * @param {HTMLElement} parent - The parent of an HTML element.
 */
function cleanList(parent) {
  while (parent.firstChild) {
    parent.removeChild(parent.firstChild);
  }
}

/**
 * Get HTML of a list item.
 * @param {Object} listItemData - The data object of the list item.
 */
function getListItem(listItemData) {
  let listItem = document.createElement("li");
  listItem.classList.add("news-list-item");
  let cardContainer = document.createElement("div");
  cardContainer.classList.add("card");

  let mediaLeft = document.createElement("div");
  mediaLeft.classList.add("media-left");
  let figure = document.createElement("figure");
  figure.classList.add("image");
  let img = document.createElement("img");
  img.src = listItemData.image;
  img.alt = "Alt";

  figure.appendChild(img);
  mediaLeft.appendChild(figure);
  cardContainer.appendChild(mediaLeft);

  let mediaRight = document.createElement("div");
  mediaRight.classList.add("media-right");

  let dateContainer = document.createElement("div");
  dateContainer.classList.add("card-date");
  let date = document.createTextNode(listItemData.date);
  dateContainer.appendChild(date);

  let titleContainer = document.createElement("div");
  titleContainer.classList.add("card-title");
  let title = document.createTextNode(listItemData.title);
  titleContainer.appendChild(title);

  let textContainer = document.createElement("div");
  textContainer.classList.add("card-text");
  let text = document.createTextNode(listItemData.text);
  textContainer.appendChild(text);

  let moreContainer = document.createElement("div");
  moreContainer.classList.add("card-more");
  let arrowRight = document.createElement("i");
  arrowRight.classList.add("arr-right");
  moreContainer.appendChild(arrowRight);
  let link = document.createElement("a");
  link.href = listItemData.more;
  let more = document.createTextNode("mehr Informationen");
  link.appendChild(more);
  moreContainer.appendChild(link);

  mediaRight.appendChild(dateContainer);
  mediaRight.appendChild(titleContainer);
  mediaRight.appendChild(textContainer);
  mediaRight.appendChild(moreContainer);

  cardContainer.appendChild(mediaLeft);
  cardContainer.appendChild(mediaRight);

  listItem.appendChild(cardContainer);

  return listItem;
}

/**
 * Selects a tab.
 * @param {MouseEvent} event - The event when a tab has been clicked.
 * @param {string} tab - The tab element.
 */
function selectTab(event, tab) {
  // console.log(event);
  // console.log(tab);
  const container = document.querySelector(`#${tab}-news-list`);
  cleanList(container);

  var i;

  for (i = 0; i < tabItems.length; i++) {
    tabItems[i].className = tabItems[i].className.replace(" is-active", "");
  }

  for (i = 0; i < tabContent.length; i++) {
    tabContent[i].style.display = "none";
  }

  document.getElementById(tab).style.display = "block";
  event.currentTarget.className += " is-active";

  loadList(tab);
}

/**
 * Gets a random tab.
 * @param {number} tabCount - The amount of tabs.
 */
function getRandomTab(tabCount) {
  return Math.floor(Math.random() * Math.floor(tabCount));
}

// Click the element with id="defaultTab"
// const defaultTab = document.getElementById("default");
// defaultTab.click();


// Click a random tab
// console.log(tabItems);
tabItems[getRandomTab(tabItems.length)].click();
